This Repository was created by Justin Harvey for Conestoga College INFO2300 Project Development course. Instructor - Rick Guzik

The majority of this project is already built as it is a simple html webpage. As long as the subfolders (pages, images and css) are located in the same direcotry as
the index.html simply opening the index.html will launch the website to be viewed within the browser. Right clicking the index.html file the user can also
select their browser of choice in which to view the webpage.

The MIT liscense was selected as this project can be open source it is merely used as an example to assist with learning about AODA standards and is not designed for
profit or to be marketed.